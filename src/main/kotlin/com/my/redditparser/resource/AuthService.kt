package com.my.redditparser.resource

import com.fasterxml.jackson.databind.ObjectMapper
import com.my.redditparser.model.exception.RedditAuthException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.io.IOException

@Service
class AuthService(private val restTemplate: RestTemplate, private val objectMapper: ObjectMapper) {
    private val log = LoggerFactory.getLogger(AuthService::class.java)

    @Value("\${reddit.auth.app.id}")
    private lateinit var appId: String

    @Value("\${reddit.auth.app.secret}")
    private lateinit var appSecret: String

    @Value("\${reddit.auth.url}")
    private lateinit var authUrl: String

    private var token: String? = null

    fun getAuthToken(): String {
        if (token == null) {
            try {
                refreshToken()
            } catch (e: IOException) {
                throw RedditAuthException("Failed to get Bearer token", e)
            }
        }

        return this.token!!
    }

    @Throws(IOException::class)
    fun refreshToken() {
        val headers = HttpHeaders()
        headers.setBasicAuth(appId, appSecret)
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED
        headers[HttpHeaders.USER_AGENT] = listOf("java:com.my.redditparser:v0.1.0 (by /u/mefz)")
        val body = "grant_type=client_credentials"
        val request = HttpEntity(body, headers)

        val response = restTemplate.postForEntity(authUrl, request, String::class.java)

        assert(response.statusCode == HttpStatus.OK)
        val accessToken = objectMapper.readTree(response.body)["access_token"].asText()
        log.info("### Got access token")

        this.token = accessToken
    }
}