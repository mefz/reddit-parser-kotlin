package com.my.redditparser.resource

import com.jayway.jsonpath.JsonPath
import com.my.redditparser.model.exception.RedditAuthException
import com.my.redditparser.model.exception.RequestFailedException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class RedditPoller(private val authService: AuthService, private val restTemplate: RestTemplate) {
    private val log = LoggerFactory.getLogger(RedditPoller::class.java)

    @Value("\${reddit.feed.url}")
    private lateinit var url: String

    @Value("\${reddit.feed.user-agent}")
    private lateinit var userAgent: String

    @Value("\${reddit.feed.messages-per-topic}")
    private lateinit var postsLimit: Number

    fun poll(topic: String, nextPageId: String = ""): String {
        val headers = HttpHeaders()
        try {
            headers.setBearerAuth(authService.getAuthToken())
        } catch (e: RedditAuthException) {
            throw RequestFailedException("Failed to get Authorization token from Reddit")
        }
        log.info("### Requesting subreddit: {}#{}", topic, nextPageId)
        headers[HttpHeaders.USER_AGENT] = listOf(userAgent)
        val request = HttpEntity<String>(headers)

        val responseEntity = restTemplate.exchange(requestUrl(topic, nextPageId),
                HttpMethod.GET, request, String::class.java)
        if ((responseEntity.statusCode != HttpStatus.OK) || !responseEntity.hasBody()) {
            log.warn("### Failed to get data for topic:{}#{}. Response code is {}",
                    topic, nextPageId, responseEntity.statusCode)
            throw RequestFailedException("Failed to get data for topic:$topic#$nextPageId")
        }

        return responseEntity.body.toString()
    }

    fun pollNextPage(topic: String, previousResult: String): String? {
        val nextPageId: String? = JsonPath.parse(previousResult).read("$.data.after")
        return if (nextPageId !=null) poll(topic, nextPageId) else null
    }

    private fun requestUrl(topic: String, nextPageId: String): String {
        val nextPageRequest = if (nextPageId.isEmpty()) nextPageId else "&after=$nextPageId"

        return "$url/search.json?q=subreddit:$topic&include_over_18=on&sort=top&t=day&limit=$postsLimit$nextPageRequest"
    }

}
