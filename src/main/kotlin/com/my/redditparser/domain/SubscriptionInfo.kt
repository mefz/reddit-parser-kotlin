package com.my.redditparser.domain

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.index.Indexed
import java.io.Serializable
import java.time.LocalDate
import java.util.*

@RedisHash("subscriptions")
data class SubscriptionInfo(@Indexed val chatId: Long, @Indexed val topic: String): Serializable {

    @Id
    var id: UUID = UUID.randomUUID()

    var fetchedAt: LocalDate = LocalDate.now()
}