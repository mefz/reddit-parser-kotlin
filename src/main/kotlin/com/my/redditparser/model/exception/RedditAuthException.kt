package com.my.redditparser.model.exception

class RedditAuthException(message: String, cause: Throwable) : RuntimeException(message, cause)
