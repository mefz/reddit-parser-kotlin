package com.my.redditparser.model.exception

class RequestFormatException(cause: Throwable) : Exception(cause)
