package com.my.redditparser.model.exception

class RequestFailedException(message: String) : Exception(message)
