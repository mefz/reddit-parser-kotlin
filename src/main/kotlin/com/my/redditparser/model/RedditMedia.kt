package com.my.redditparser.model

import org.telegram.telegrambots.meta.api.methods.send.SendAnimation
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.methods.send.SendVideo

data class RedditMedia(val caption: String, val url: String, val type: MediaType) {
    fun toTelegramMessage(): Any = when (type) {
        MediaType.IMAGE -> SendPhoto().setCaption(caption).setPhoto(url)
        MediaType.VIDEO -> SendVideo().setCaption(caption).setVideo(url)
        MediaType.GIF -> SendAnimation().setCaption(caption).setAnimation(url)
    }

}