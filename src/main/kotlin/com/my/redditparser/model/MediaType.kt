package com.my.redditparser.model

enum class MediaType {
    IMAGE, VIDEO, GIF
}