package com.my.redditparser.typeparsers

import com.jayway.jsonpath.DocumentContext
import com.jayway.jsonpath.JsonPath
import com.my.redditparser.model.MediaType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

@Component
class CrosspostParser: TypeParser {

    @Lazy
    @Autowired
    private lateinit var parsers: List<TypeParser>

    override fun isType(elementContext: DocumentContext) =
            ("link" == elementContext.read("$.post_hint"))
                    && (elementContext.read("$.crosspost_parent_list.length()", Int::class.java)?:0 > 0)

    override fun getMedia(elementContext: DocumentContext): String {
        val wrappedElement = wrapCrosspostElement(elementContext)

        return findParser(wrappedElement)?.getMedia(wrappedElement).orEmpty()
    }

    override fun getType(elementContext: DocumentContext): MediaType {
        val wrappedElement = wrapCrosspostElement(elementContext)

        return findParser(wrappedElement)?.getType(wrappedElement)?: MediaType.IMAGE
    }

    private fun findParser(elementContext: DocumentContext): TypeParser? =
            parsers.find { it.isType(elementContext) }

    private fun wrapCrosspostElement(elementContext: DocumentContext) =
            JsonPath.using(TypeParser.configuration)
                    .parse(elementContext.read("$.crosspost_parent_list[0]", Any::class.java))

}
