package com.my.redditparser.typeparsers

import com.jayway.jsonpath.DocumentContext
import com.my.redditparser.model.MediaType
import org.springframework.stereotype.Component

@Component
class LinkGifvParser: TypeParser {
    private val extension = ".gifv"

    override fun isType(elementContext: DocumentContext): Boolean {
        val url = elementContext.read("$.url", String::class.java)

        return url?.toLowerCase() == extension
    }

    override fun getMedia(elementContext: DocumentContext): String {
        val originalUrl = super.getMedia(elementContext)

        return originalUrl.substring(0, originalUrl.length - extension.length)
    }

    override fun getType(elementContext: DocumentContext) = MediaType.VIDEO

}