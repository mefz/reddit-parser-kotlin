package com.my.redditparser.typeparsers

import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.DocumentContext
import com.jayway.jsonpath.Option
import com.my.redditparser.model.MediaType
import com.my.redditparser.model.RedditMedia

interface TypeParser {
    companion object {
        val configuration: Configuration = Configuration.defaultConfiguration()
                .addOptions(Option.SUPPRESS_EXCEPTIONS)
    }
    fun isType(elementContext: DocumentContext): Boolean
    fun getMedia(elementContext: DocumentContext): String {
        return elementContext.read("$.url")
    }

    fun getCaption(elementContext: DocumentContext): String {
        return elementContext.read("$.title")
    }

    fun getType(elementContext: DocumentContext): MediaType

    fun getRedditMedia(elementContext: DocumentContext): RedditMedia =
            RedditMedia(
                    getCaption(elementContext),
                    getMedia(elementContext),
                    getType(elementContext)
            )
}