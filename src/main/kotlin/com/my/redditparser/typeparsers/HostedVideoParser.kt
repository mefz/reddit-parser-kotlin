package com.my.redditparser.typeparsers

import com.jayway.jsonpath.DocumentContext
import com.my.redditparser.model.MediaType
import org.springframework.stereotype.Component

@Component
class HostedVideoParser: TypeParser {
    override fun isType(elementContext: DocumentContext) =
            "hosted:video" == elementContext.read("$.post_hint")

    override fun getMedia(elementContext: DocumentContext): String =
            elementContext.read("$.media.reddit_video.fallback_url")

    override fun getType(elementContext: DocumentContext) = MediaType.VIDEO

}