package com.my.redditparser.typeparsers

import com.jayway.jsonpath.DocumentContext
import com.my.redditparser.model.MediaType
import org.springframework.stereotype.Component

@Component
class ImageParser: TypeParser {
    override fun isType(elementContext: DocumentContext) =
            "image" == elementContext.read("$.post_hint")

    override fun getType(elementContext: DocumentContext) = MediaType.IMAGE
}