package com.my.redditparser.service

import com.my.redditparser.model.RedditMedia
import com.my.redditparser.model.exception.RequestFailedException
import com.my.redditparser.resource.RedditPoller
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Service
class FeedService(
        private val subscriptionService: SubscriptionService,
        private val redditPoller: RedditPoller,
        private val mediaParser: MediaParser,
        private val sendingService: SendingService) {
    private val log = LoggerFactory.getLogger(FeedService::class.java)

    @Value("\${reddit.feed.messages-per-topic}")
    private lateinit var postsLimit: Number

    @Value("\${reddit.feed.max-queries}")
    private lateinit var maxQueries: Number

    fun processUnread() {
        var fetchedData: String
        subscriptionService.getAllUnlisted().forEach { subscription ->
            try {
                fetchedData = redditPoller.poll(subscription.topic)
                val mediaList: MutableList<RedditMedia> = mediaParser.parse(fetchedData).toMutableList()

                var queries = 1
                while ((mediaList.size < postsLimit.toInt()) && (queries++ < maxQueries.toInt())) {
                    fetchedData = redditPoller.pollNextPage(subscription.topic, fetchedData) ?: break
                    mediaList.addAll(mediaParser.parse(fetchedData))
                }

                mediaList.stream()
                        .limit(postsLimit.toLong())
                        .map { it.toTelegramMessage() }
                        .forEach { message -> sendingService.send(message, subscription.chatId) }

                subscriptionService.updateFetchDate(subscription)
            } catch (e: RequestFailedException) {
                log.warn("### Error getting the {} feed", subscription.topic)
                sendingService.send(
                        SendMessage().setText("There was a problem with getting the ${subscription.topic} feed"),
                        subscription.chatId)
            }
        }
    }
}