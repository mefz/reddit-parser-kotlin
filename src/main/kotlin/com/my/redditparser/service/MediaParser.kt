package com.my.redditparser.service

import com.my.redditparser.model.RedditMedia

interface MediaParser {
    fun parse(sourceData: String): List<RedditMedia>
}