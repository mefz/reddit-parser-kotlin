package com.my.redditparser.service.telegram

import com.my.redditparser.service.SendingService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.methods.send.SendVideo
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import javax.annotation.PostConstruct

@Service
class TelegramBot(private val controller: TelegramCommandController) : TelegramLongPollingBot(), SendingService {

    @Value("\${telegram.bot.name}")
    private lateinit var botName: String

    @Value("\${telegram.bot.token}")
    private lateinit var botToken: String

    private val log = LoggerFactory.getLogger(TelegramBot::class.java)
    private val commandHandlers: MutableMap<String, Method> = mutableMapOf()

    @PostConstruct
    private fun initCommandHandlers() {
        var annotation: CommandHandler?
        for (method in controller.javaClass.declaredMethods) {
            annotation = method.getDeclaredAnnotation(CommandHandler::class.java)
            if (annotation != null) {
                if (Modifier.isPrivate(method.modifiers))
                    throw IllegalAccessException("@CommandHandler cannot be set on a private method " +
                            "(${controller.javaClass.canonicalName}$${method.name})")
                commandHandlers[annotation.command] = method
            }
        }
    }

    @PostConstruct
    private fun registerBot() {
        val botsApi = TelegramBotsApi()
        try {
            botsApi.registerBot(this)
        } catch (e: TelegramApiRequestException) {
            e.printStackTrace()
        }
        log.info("### Telegram bot has been created")
    }

    override fun getBotToken() = botToken

    override fun getBotUsername() = "@$botName"

    override fun onUpdateReceived(update: Update) {
        val message = update.message?.text.orEmpty()
        val chatId = update.message?.chatId

        if (chatId == null) {
            log.error("### Provided NULL chatId parameter!")
            return
        }

        if (message.startsWith('/')) {
            val command = message.substringBefore(':')

            val topic = message.substringAfter(':', "incorrect format")
            val parameters = arrayOf(chatId, topic)

            val handler = commandHandlers[command]
            if (handler != null) {
                val reply = handler.invoke(controller,
                        *parameters.copyOfRange(0, commandHandlers[command]?.parameterCount ?: 0))
                send(reply, chatId)
            }
        }
    }

    override fun send(message: Any?, chatId: Long) {
        try {
            when(message) {
                is SendMessage -> {
                    message.setChatId(chatId)
                    execute(message)
                }
                is SendPhoto -> {
                    message.setChatId(chatId)
                    execute(message)
                }
                is SendVideo -> {
                    message.setChatId(chatId)
                    execute(message)
                }
                is SendAnimation -> {
                    message.setChatId(chatId)
                    execute(message)
                }
                else -> execute(SendMessage(chatId, "Unknown reply message type"))
            }
        } catch (e: TelegramApiRequestException) {
            log.warn("### Failed to send message:${message} to chat:$chatId")
        }
    }

}
