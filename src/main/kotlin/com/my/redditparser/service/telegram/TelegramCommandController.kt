package com.my.redditparser.service.telegram

import com.my.redditparser.service.FeedService
import com.my.redditparser.service.SubscriptionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import org.telegram.telegrambots.meta.api.methods.send.SendMessage

@Service
class TelegramCommandController(private val subscriptionService: SubscriptionService) {

    @Autowired
    @Lazy
    private lateinit var feedService: FeedService

    @CommandHandler("/subscribe")
    fun subscribe(chatId: Long, topic: String): SendMessage {
        val message = SendMessage()

        if (topic == "incorrect format") {
            message.text = "Incorrect command format"
        } else {
            message.text = if (subscriptionService.addSubscription(chatId, topic)) "You've subscribed to $topic"
            else "You're already subscribed to $topic"
        }

        return message
    }

    @CommandHandler("/subscriptions")
    fun subscriptions(chatId: Long): SendMessage {
        val message = SendMessage()
        message.text = subscriptionService.getChatSubscriptions(chatId).joinToString("\n")

        return message
    }

    @CommandHandler("/unsubscribe")
    fun unsubscribe(chatId: Long, topic: String): SendMessage {
        val message = SendMessage()

        if (topic == "incorrect format") {
            message.text = "Incorrect command format"
        } else {
            message.text = if (subscriptionService.removeSubscription(chatId, topic)) "You've unsubscribed from $topic"
            else "You're not subscribed to $topic"
        }

        return message
    }

    //TODO Delete after test period
    /**
     * For testing purposes
     */
    @CommandHandler("/poll")
    fun poll(chatId: Long): SendMessage {
        feedService.processUnread()

        return SendMessage().setText("done")
    }
}
