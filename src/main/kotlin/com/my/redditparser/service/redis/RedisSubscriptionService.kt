package com.my.redditparser.service.redis

import com.my.redditparser.domain.SubscriptionInfo
import com.my.redditparser.repository.SubscriptionsRepository
import com.my.redditparser.service.SubscriptionService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class RedisSubscriptionService(private val subscriptionsRepository: SubscriptionsRepository) : SubscriptionService {

    private val log = LoggerFactory.getLogger(RedisSubscriptionService::class.java)

    /**
     * @return true if added a new subscription, otherwise, if one already exists, false
     */
    override fun addSubscription(chatId: Long, topic: String): Boolean {
        return if (subscriptionsRepository.findByChatIdAndTopic(chatId, topic) == null) {
            subscriptionsRepository.save(SubscriptionInfo(chatId, topic))
            log.info("### Chat {} has been subscribed to {}", chatId, topic)
            true
        } else {
            log.info("### Chat {} is already subscribed to {}", chatId, topic)
            false
        }
    }

    /**
     * @return true if subscription exists and it's been removed, otherwise, if not subscribed, false
     */
    override fun removeSubscription(chatId: Long, topic: String): Boolean {
        val subscription = subscriptionsRepository.findByChatIdAndTopic(chatId, topic)
        return if (subscription != null) {
            subscriptionsRepository.delete(subscription)
            log.info("### Chat {} has been unsubscribed from {}", chatId, topic)
            true
        } else {
            log.info("### Chat {} is not subscribed to {}", chatId, topic)
            false
        }
    }

    override fun getAllUnlisted(): List<SubscriptionInfo> {
        val dateToday = LocalDate.now()

        return subscriptionsRepository.findAll()
                .filter { it.fetchedAt.isBefore(dateToday) }
                .toList()
    }

    override fun getChatSubscriptions(chatId: Long): List<String> =
            subscriptionsRepository.findByChatId(chatId)
                    .map(SubscriptionInfo::topic)
                    .ifEmpty { listOf("No subscriptions") }

    override fun updateFetchDate(subscription: SubscriptionInfo) {
        subscription.fetchedAt = LocalDate.now()
        subscriptionsRepository.save(subscription)
    }

}