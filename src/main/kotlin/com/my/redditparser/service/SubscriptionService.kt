package com.my.redditparser.service

import com.my.redditparser.domain.SubscriptionInfo

interface SubscriptionService {
    /**
     * @return true if added a new subscription, otherwise, if one already exists, false
     */
    fun addSubscription(chatId: Long, topic: String): Boolean

    fun getAllUnlisted(): List<SubscriptionInfo>

    fun updateFetchDate(subscription: SubscriptionInfo)

    /**
     * @return true if subscription exists and it's been removed, otherwise, if not subscribed, false
     */
    fun removeSubscription(chatId: Long, topic: String): Boolean
    fun getChatSubscriptions(chatId: Long): List<String>
}
