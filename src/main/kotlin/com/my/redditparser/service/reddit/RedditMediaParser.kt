package com.my.redditparser.service.reddit

import com.jayway.jsonpath.DocumentContext
import com.jayway.jsonpath.JsonPath
import com.my.redditparser.model.RedditMedia
import com.my.redditparser.service.MediaParser
import com.my.redditparser.typeparsers.TypeParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RedditMediaParser : MediaParser {

    @Autowired
    private lateinit var typeParsers: List<TypeParser>

    override fun parse(sourceData: String): List<RedditMedia> {
        val sourceContext = JsonPath.using(TypeParser.configuration).parse(sourceData)
        val elements: List<DocumentContext> = sourceContext.read("$..children[*].data", List::class.java)
                .mapNotNull { JsonPath.using(TypeParser.configuration).parse(it) }

        return elements.mapNotNull { convertElement(it) }
    }

    private fun convertElement(elementContext: DocumentContext): RedditMedia? =
            typeParsers.find { it.isType(elementContext) }?.getRedditMedia(elementContext)

}
