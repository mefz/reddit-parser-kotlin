package com.my.redditparser.service

interface SendingService {
    fun send(message: Any?, chatId: Long)
}