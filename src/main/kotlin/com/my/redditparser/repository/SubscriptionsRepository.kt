package com.my.redditparser.repository

import com.my.redditparser.domain.SubscriptionInfo
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SubscriptionsRepository : CrudRepository<SubscriptionInfo, UUID> {
    fun findByChatIdAndTopic(chatId: Long, topic: String): SubscriptionInfo?
    fun findByChatId(chatId: Long): Set<SubscriptionInfo>
}