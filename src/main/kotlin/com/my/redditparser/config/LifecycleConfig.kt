package com.my.redditparser.config

import com.my.redditparser.service.FeedService
import com.my.redditparser.service.SendingService
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener

@Configuration
@Profile("!dev")
class LifecycleConfig(private val feedService: FeedService, private val sendingService: SendingService) {

    @Value("\${telegram.bot.info-chat-id}")
    private lateinit var infoChatId: String

    @Bean
    fun servletListener() : ServletListenerRegistrationBean<ServletContextListener> {
        val servletListener = ServletListenerRegistrationBean<ServletContextListener>()
        servletListener.listener =
                RunningServiceServletContextListener(feedService, sendingService, infoChatId.toLong())

        return servletListener
    }

    private class RunningServiceServletContextListener(
            private val feedService: FeedService,
            private val sendingService: SendingService,
            private val infoChatId: Long) : ServletContextListener {

        override fun contextInitialized(sce: ServletContextEvent?) {
            super.contextInitialized(sce)
            sendingService.send(SendMessage().setText("Reddit Parser bot is up"), infoChatId)
            feedService.processUnread()
        }

        override fun contextDestroyed(sce: ServletContextEvent?) {
            super.contextDestroyed(sce)
            sendingService.send(SendMessage().setText("Reddit Parser bot is down"), infoChatId)
        }
    }
}