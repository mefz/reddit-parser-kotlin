package com.my.redditparser.config

import io.lettuce.core.RedisURI
import io.lettuce.core.resource.ClientResources
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.StringRedisSerializer

@Configuration
class RedisConfig(private val clientResources: ClientResources) {

    @Value("\${REDIS_URL}")
    private val redisUrl: String? = null

    @Bean
    fun lettuceConnectionFactory(): LettuceConnectionFactory {
        val redisUri = RedisURI.create(redisUrl)
        val configuration = RedisStandaloneConfiguration(redisUri.host, redisUri.port)
        configuration.setPassword(redisUri.password)

        val lettuceClientConfiguration = LettuceClientConfiguration.builder()
                .clientResources(clientResources)
                .build();
        val factory = LettuceConnectionFactory(configuration, lettuceClientConfiguration)
        factory.afterPropertiesSet()

        return factory
    }

    @Bean
    fun redisTemplate(lettuceConnectionFactory: LettuceConnectionFactory): RedisTemplate<String, Any> {
        val redisTemplate = RedisTemplate<String, Any>()
        redisTemplate.setConnectionFactory(lettuceConnectionFactory)
        redisTemplate.keySerializer = StringRedisSerializer()
        redisTemplate.valueSerializer = GenericJackson2JsonRedisSerializer()
        redisTemplate.hashKeySerializer = GenericJackson2JsonRedisSerializer()

        return redisTemplate
    }
}