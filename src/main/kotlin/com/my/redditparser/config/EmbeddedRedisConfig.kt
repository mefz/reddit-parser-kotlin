package com.my.redditparser.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import redis.embedded.RedisServer
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Configuration
@Profile("dev")
class EmbeddedRedisConfig {
    private val redisServer: RedisServer = RedisServer()

    @PostConstruct
    fun start() {
        redisServer.start()
    }

    @PreDestroy
    fun stop() {
        redisServer.stop()
    }

}
